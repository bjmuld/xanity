#!/bin/bash
set -e -x

#############################################################################
#############################################################################
###
###
###         REQUIRES:  
###                    $PKG_NAME
###
###         THIS WILL BUILD A WHEEL FOR PYTHON SYSTEM FOUND AT $PYBIN
###
###
#############################################################################
#############################################################################

WHEEL_DIR="${1}"
export TWINE_NON_INTERACTIVE=1
pip install .
XANITY_VER="$(python -m xanity --version | cut -f1 -d ' ')"
PUB_VER="$(yolk -V xanity | cut -f2 -d' ')"
if [[ "${PUB_VER}" != "${XANITY_VER}" ]]; then
    python -m keyring --disable
    twine check "${WHEEL_DIR}/*"
    twine upload --verbose "${WHEEL_DIR}/*"
fi
