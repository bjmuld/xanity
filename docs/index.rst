.. Xanity documentation master file, created by
   sphinx-quickstart on Mon Dec 21 16:05:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Xanity's documentation!
==================================
.. include README.md
.. automodule:: xanity
.. autoclass:: xanity.class_obj.XanityClass
.. autofunction:: xanity.xanity_run::main

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
