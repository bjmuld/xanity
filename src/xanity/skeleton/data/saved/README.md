## <project-root>/data/saved/

# xanity automation:  
 - None

# git automation:
 - This directory **is tracked by git**

# summary:

Move valuable runs from /data/ to here in order to save the results.
This *must* be done manually
