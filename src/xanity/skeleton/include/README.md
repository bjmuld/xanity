## <project-root>/include/

#### no automation here
This directory **is tracked by git**

This directory **is tarballed into the `run_data` directory** at every run.

***

This is a place for non-python source code or data which is required for runs,
but is not the subject of experimentation.  That is to say, mostly unchanging
throughout development.

For example:
 - ./spice/
 - ./images/
 - ./maps/