## Description of project

Use this file to describe your experiment:

eg:

#### optimizers used:
- Machine Learning:
    1. DDPG
    2. DQN
- Random Agent
- NLopt