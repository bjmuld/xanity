#!/usr/bin/env bash

set -e -x

echo '
######################################################
##
##     list wheelhouse
##
######################################################'
test -d "${CI_PROJECT_DIR}/wheelhouse" && ls -alh "${CI_PROJECT_DIR}/wheelhouse"





echo '
######################################################
##
##     install system requirements
##
######################################################'
apt-get update
apt-get install -y --no-install-recommends rsync wget git


echo '
######################################################
##
##      install pip requirents
##
######################################################'
#pip install "${CI_PROJECT_DIR}"/wheelhouse/xanity-*-py2.py3-none-any.whl
pip install pytest

test -d "${CI_PROJECT_DIR}" && {
  pip install "${CI_PROJECT_DIR}"/wheelhouse/xanity-*-py2.py3-none-any.whl || pip install "${CI_PROJECT_DIR}" ;
} || { test -d /xanity && pip install /xanity; }




echo '
######################################################
## 
##      install conda
##
######################################################'
test ! -d /root/miniconda3/ && { \
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O instmini.sh ;
chmod +x instmini.sh ;
./instmini.sh -bfu ;
. /root/miniconda3/etc/profile.d/conda.sh ;
conda init --all ;
}




echo '
######################################################
##
##    debugging before running tests
##
######################################################'







echo '
######################################################
##
##    run tests
##
######################################################'
test -d "${CI_PROJECT_DIR}" && pytest "${CI_PROJECT_DIR}" || {
  pytest /xantiy;
}