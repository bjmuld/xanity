import pytest

from .classes import *
from .units import UnitTests


@pytest.mark.incremental
class Test:

    TestFullDemo()

    TestAdditionalFeatures()
