## pytest config
import pytest

collect_ignore = ['units']


@pytest.fixture(scope="session")
def proj1_root(tmp_path_factory):
    fn = tmp_path_factory.mktemp('test_project1')
    return fn


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            pytest.xfail("previous test failed ({})".format(previousfailed.name))


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "incremental: mark class to run incrementally"
    )
