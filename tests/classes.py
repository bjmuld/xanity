import os
import pytest
from .common import bash


@pytest.mark.incremental
class TestFullDemo:

    def test_init(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity init --examples --debug --debug-xanity -v 99')

    def test_setup(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity env setup --debug --debug-xanity -v 99')

    def test_run(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity run --force --debug --checkpoints --debug-xanity -v 99')

    def test_data(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity data summarize --debug --debug-xanity -v 99')


@pytest.mark.incremental
class TestAdditionalFeatures:

    def test_purge(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity data purge cache --debug --debug-xanity -v 99')

    def test_run_again(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity run --force --debug --debug-xanity -v 99')

    def test_data_again(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity data summarize --debug --debug-xanity -v 99')

    def test_remove_env(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity env remove --debug --debug-xanity -v 99')

    def test_remake_env(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity env setup --debug --debug-xanity -v 99')

    def test_run_again2(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity run --force --debug --checkpoints --debug-xanity -v 99')

    def test_setup_over_existing(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity env setup --debug --debug-xanity -v 99')

    def test_run_again3(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity run --force --debug --checkpoints --debug-xanity -v 99')

    def test_data_again2(self, proj1_root, capfd):
        os.chdir(str(proj1_root))
        assert not bash('xanity data summarize --debug --debug-xanity -v 99')


