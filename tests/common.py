import subprocess
from os import path
import os

from shlex import split as shsplit


def bash(command):
    return subprocess.call(shsplit('bash -lc \'{}\''.format(command)))


def env_bash(command):
    # # assumes you're in the root of a xanity project
    rcfile = path.join(os.getcwd(), '.xanity', 'bashrc')
    assert path.isfile(rcfile)
    return subprocess.call(shsplit('bash --rcfile {} -lc \'{}\''.format(rcfile, command)))
