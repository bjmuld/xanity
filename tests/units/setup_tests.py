import os
import xanity
from ..common import *


def test_setup(proj1_root, capfd):

	os.chcwd(proj1_root)
	xanity.new_xanity_session()

	xanity.cmd('env setup')

	assert 'created' in capfd.stdout
	assert os.isfile(os.path.join('.xanity', 'setupcomplete'))
