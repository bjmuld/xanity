import os
import xanity

from ..common import *


def test_run(proj1_root, syscap):

	os.chdir(proj1_root)
	xanity.new_xanity_session()

	treetop1 = bash('ls -a data/runs')
	tree1 = bash('ls -ar data/runs')

	xanity.cmd('run')

	treetop2 = bash('ls -a data/runs')
	tree2 = bash('ls -ar data/runs')

	assert 'created' in msg
	assert treetop1 != treetop2
	assert tree1 != tree2
