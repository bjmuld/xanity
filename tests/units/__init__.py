from . import init_tests
from . import run_tests
from . import setup_tests


class UnitTests:

    init_tests.test_init
    run_tests.test_run
    setup_tests.test_setup