import pytest
import os
import xanity

from ..common import *


def test_init(proj1_root):

	os.chdir(proj1_root)
	xanity.new_xanity_session()

	with pytest.raises(SystemExit) as excinfo:
		xanity.cmd('init --examples')

	assert excinfo.value.code == 0
	# assert 'created' in capsys.readouterr().out
	# assert ls_skel_diff(proj_name) == ''
